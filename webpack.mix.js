const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const publicJsDir = 'public/js';
const resourcesJsDir = 'resources/js';
const publicJCssDir = 'public/css';
const resourcesCssDir = 'resources/sass';

mix.js(resourcesJsDir + '/app.js', publicJsDir)
    .js(resourcesJsDir + '/dashboard.js', publicJsDir)
    .sass(resourcesCssDir + '/app.scss', publicJCssDir);
