@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('dashboard.top_coins') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <div class="js-top-coins"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('additionalHeaders')
    <script type="text/javascript">
        var getTopCoinsUrl = '{{ route("top-coins") }}';
        var topCoinsApiUrl = '{{ App\Helpers\CryptocompareHelper::URL_API }}';
    </script>
    <script src="{{ asset('plugins/DataTables/datatables.min.js') }}" type="text/javascript"></script>
    <link href="{{ asset('plugins/DataTables/datatables.min.css') }}" rel="stylesheet">

    <script src="{{ asset('js/dashboard.js') }}" type="text/javascript"></script>
@endsection