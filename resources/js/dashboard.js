$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: getTopCoinsUrl,
        success: function(response){
            if (response.success) {
                drawTopCoinsDatatable(response.data);
            } else {
                console.log('Something went wrong.');
            }
        },
        error: function () {
            console.log('Something went wrong.');
        }
    });
});

function drawTopCoinsDatatable(data) {
    $('.js-top-coins').append(generateTable(data));
    $('#coins-results thead').append('<tr><th>Iso Code</th><th>Name</th><th></th><th></th></tr>');
    $('#coins-results thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        if (title=='Iso Code' || title=='Name') {
            $(this).html('<input type="text" placeholder="Search '+title+'" />');
            $('input', this).on('keyup change', function () {
                if ( table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            });
        }
    });

    var table = $('#coins-results').DataTable({
        orderCellsTop: true,
        fixedHeader: true
    });
}

function generateTable(data) {
    var tableContent = '<table id="coins-results">';
    tableContent += '<thead><tr>';
    tableContent += '<th>Iso Code</th>';
    tableContent += '<th>Name</th>';
    tableContent += '<th>Image</th>';
    tableContent += '<th>TotalVolume24H</th>';
    tableContent += '</tr></thead><tbody>';
    for (var i = 0; i < data.length; i++) {
        tableContent += '<tr>';
        tableContent += '<td>' + data[i].iso_code + '</td>';
        tableContent += '<td>' + data[i].name + '</td>';
        tableContent += '<td><img style="height: 50px" src="' + topCoinsApiUrl + data[i].image + '"/></td>';
        tableContent += '<td>' + data[i].volume_24h + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody></table>';
    return tableContent;
}
