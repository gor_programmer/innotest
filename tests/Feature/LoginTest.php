<?php

namespace Tests\Feature;

use App\Eloquent\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class LoginTest extends TestCase
{
    const EMPTY_STRING = 'empty';

    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->baseUrl = request()->root();
        $this->user = factory(User::class)->create();
    }

    public function tearDown(): void
    {
        User::destroy($this->user->id);
    }

    /**
     * Test Login functionality
     * Class LoginTest
     * @param $responseMessage string
     * @param $password string
     * @param $name string
     * @param $customHTTP bool
     * @param $exception bool
     * @package Tests\Feature
     * @group  LoginTest
     * @return void
     * @dataProvider providerGetUserData
     */
    public function testLogin(string $name, string $password, string $responseMessage, bool $customHTTP = false, bool $exception = false)
    {
        $name = $name !== self::EMPTY_STRING ? $name : $this->user->username;
        $password = $password !== self::EMPTY_STRING ? $password : 'password';
        $responseMessage = $responseMessage !== self::EMPTY_STRING ? $responseMessage : $this->user->username;
        if (!$customHTTP) {
            $this->visit('/login')
                ->type($name, 'username')
                ->type($password, 'password')
                ->press('Login')
                ->see($responseMessage);
        } else {
            $response = $this->call('POST', '/login', [
                'username' => $name,
                'password' => $password
            ]);

            $this->assertResponseStatus(302);
            $this->assertInstanceOf(RedirectResponse::class, $response);
            if (!$exception) {
                $this->assertRedirectedTo('/dashboard');
                $this->assertEquals(null, $response->exception);
            } else {
                $this->assertRedirectedTo('/');
                $this->assertInstanceOf(ValidationException::class, $response->exception);
            }
        }
    }

    public function providerGetUserData()
    {
        return [
            [
                'name' => 'Test Name',
                'password' => '',
                'responseMessage' => 'The password field is required'
            ],
            [
                'name' => '',
                'password' => 'Test Password',
                'responseMessage' => 'The username field is required.'
            ],
            [
                'name' => 'Test Name',
                'password' => 'Test Password',
                'responseMessage' => 'These credentials do not match our records.'
            ],
            [
                'name' => self::EMPTY_STRING,
                'password' => self::EMPTY_STRING,
                'responseMessage' => self::EMPTY_STRING,
                'customHTTP' => true
            ],
            [
                'name' => self::EMPTY_STRING,
                'password' => 'Test Password',
                'responseMessage' => self::EMPTY_STRING,
                'customHTTP' => true,
                'exception' => true
            ]
        ];
    }
}
