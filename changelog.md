v1.0.0

* Upload empty laravel project to git

v1.0.1

* Add .idea to gitignore

v1.1.0

* Add authentication, make login working with username and email

v1.1.1

* 1 Create test for login, create verbose seeder, create readme.md file

v1.1.2

* 2 Add changelog.md file to the project

v1.2.0

* 3 Show data from API in dashboard

v1.3.0

* 4 Leave only login and logout from laravel default auth routes

v1.3.1

* 5 Fix login issue related with trait ThrottlesLogins
