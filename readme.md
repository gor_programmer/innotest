# README #

### What is this repository for? ###

* This is the test task for InnotechSoftware
* The project is written in php [laravel 5.8](https://laravel.com/docs/5.8) framework which is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## How to get set up? ##

### Rerequirements ###
* For the server requirements please check [here](https://laravel.com/docs/5.8/installation)
* You also need to have `mysql` installed on your local machine

### Clone the project into your local machine ###

### Install laravel dependencies ###
```
composer install
```

### Create .env config file ###

```
cp .env.example .env
```

### Define your local MySQL database params in `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` settings. ###

### Generate key ###
```
php artisan key:generate
```

### Database configuration ###
```
sudo php artisan migrate
```
```
sudo php artisan db:seed
```

### Install node modules ###
```
npm install
```

### Create asset files ###
```
npm run prod
```
### Local domain ###

* Add next to systems `hosts`:

``` 
127.0.0.1 http://innotest.loc
```

* Run server:

```
php artisan serve --host innotest.loc
```

## Feel free to ask questions about any thing related with the project)