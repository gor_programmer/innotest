<?php

namespace App\Http\Controllers;

use App\Helpers\CryptocompareHelper;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('dashboard.dashboard');
    }

    public function getTopCoins()
    {
        $response = CryptocompareHelper::getTopCoins();
        return response()->json($response);
    }
}
