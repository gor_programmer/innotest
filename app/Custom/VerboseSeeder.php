<?php

namespace App\Custom;

use Illuminate\Database\Seeder;

class VerboseSeeder extends Seeder
{
    protected $actions;

    public function __construct()
    {
        $this->actions = new DefaultDict(0);
    }

    public function log($what)
    {
        $this->actions[$what] += 1;
    }

    public function __invoke()
    {
        parent::__invoke();

        $logStr = '';
        if (count($this->actions->getContainer()) == 0) {
            $logStr = '<comment>No changes</comment>';
        } else {
            foreach ($this->actions->getContainer() as $title => $value) {
                $t = ucfirst($title);
                $logStr .= "<info>|</info>$t: <comment>$value</comment>  ";
            }
        }

        $this->command->getOutput()->writeln($logStr . PHP_EOL);
    }
}
