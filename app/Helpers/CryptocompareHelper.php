<?php

namespace App\Helpers;

use Cache;

class CryptocompareHelper
{
    const URL_API = 'https://www.cryptocompare.com';
    const URL_MIN_API = 'https://min-api.cryptocompare.com/';
    const URL_PART_TOP_COINS = 'data/top/totalvol';

    const PARAM_BASE_CURRENCY = 'tsym';
    const PARAM_LIMIT = 'limit';

    const BASE_CURRENCY = 'USD';
    const LIMIT = '100';

    const CACHE_TIME = 300;

    public static function getTopCoins(): array
    {
        $topCoins = Cache::remember('topCoins', self::CACHE_TIME, function () {
            $apiUrl = self::getTopCoinsUrl();
            $topCoins = CurlHelper::getResult($apiUrl);
            $response = self::prepareTopCoinsArr($topCoins);
            return $response;
        });
        return $topCoins;
    }

    private static function getTopCoinsUrl()
    {
        return self::URL_MIN_API . self::URL_PART_TOP_COINS
                . '?' . self::PARAM_BASE_CURRENCY . '=' . self::BASE_CURRENCY
                . '&' . self::PARAM_LIMIT . '=' . self::LIMIT;
    }

    /**
     * @todo Add fields (also in next function) to constants because they could be used also in another functions
     */
    private static function prepareTopCoinsArr(string $str): array
    {
        $topCoinsArr = json_decode($str, true);
        $response = [];
        $response['data'] = [];
        if (isset($topCoinsArr['Message']) && $topCoinsArr['Message'] == 'Success') {
            $response['success'] = true;
            if (isset($topCoinsArr['Data'])) {
                foreach ($topCoinsArr['Data'] as $topCoin) {
                    $response['data'][] = self::getTopCoinsNecessaryFields($topCoin);
                }
            }
        } else {
            $response['success'] = false;
        }

        return $response;
    }

    private static function getTopCoinsNecessaryFields(array $topCoin): array
    {
        $topCoinData = [];
        $topCoinData['name'] = $topCoin['CoinInfo']['FullName'] ?? '';
        $topCoinData['iso_code'] = $topCoin['CoinInfo']['Name'] ?? '';
        $topCoinData['image'] = $topCoin['CoinInfo']['ImageUrl'] ?? '';
        $topCoinData['volume_24h'] = $topCoin['ConversionInfo']['TotalVolume24H'] ?? '';

        return $topCoinData;
    }
}
