<?php

use App\Eloquent\Models\User;
use App\Custom\VerboseSeeder;

class UsersTableSeeder extends VerboseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUsers();
    }

    private function createUsers()
    {
        $data = [
            [
                'name' => 'Test User',
                'email' => 'test@user.com',
                'username' => 'test-user',
                'password' => bcrypt('testuser'),
            ],
            [
                'name' => 'Test1 User',
                'email' => 'test1@user.com',
                'username' => 'test1-user',
                'password' => bcrypt('test1user'),
            ],
            [
                'name' => 'Test2 User',
                'email' => 'test2@user.com',
                'username' => 'test2-user',
                'password' => bcrypt('test2user'),
            ],
        ];

        foreach ($data as $userData) {
            $user = User::where('email', $userData['email'])
                ->orWhere('username',  $userData['username'])
                ->first();

            if (!$user) {
                User::create($userData);
                $this->log('Created users');
            }
        }
    }
}
